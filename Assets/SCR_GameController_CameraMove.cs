﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GameController_CameraMove : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SCR_GameController_Input ginput = GetComponent(typeof(SCR_GameController_Input)) as SCR_GameController_Input;
        Camera gcamera = (Camera) GameObject.FindObjectOfType(typeof(Camera));
        if (Camera.main.aspect >= 1.7) {
            //16:9+
            gcamera.transform.position = new Vector3(0, 4.8f, -3.6f);
            ginput.touchNum = 0.225f;
        } else if (Camera.main.aspect >= 1.5) {
            //3:2
            gcamera.transform.position = new Vector3(0, 5.2f, -4.2f);
            ginput.touchNum = 0.205f; // who knows! figure it out :)
        } else if (Camera.main.aspect >= 1.33) {
            //4:3
            gcamera.transform.position = new Vector3(0, 5.8f, -4.4f);
            ginput.touchNum = 0.1875f;
        } else if (Camera.main.aspect >= 1.25) {
            //5:4
            gcamera.transform.position = new Vector3(0, 6, -4.8f);
            ginput.touchNum = 0.175f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
