﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SimpleFileBrowser;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SCR_GameController_Audio : MonoBehaviour
{
    public float startTime = 0;

    public float bpm = 0;

    int status = -1; //-1 = stopped, 0 = paused, 1 = playing;

    bool isEditor = false;

    public float offset = 0;

    // Start is called before the first frame update
    void Start() {
        isEditor = SceneManager.GetActiveScene().name == "EditorScene";
    }

    // Update is called once per frame
    void Update() {
        if (isEditor && status == 1) { //stuff here only works when not paused
            AudioSource gaudio = GameObject.Find("song").GetComponent<AudioSource>();
            if (!gaudio.isPlaying) {
                status = 0;
                gaudio.Pause();
                (GameObject.Find("GameController").GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script).ToggleBumButtons(true);
                gaudio.time = gaudio.clip.length-BeatToSecond(4);
                startTime = gaudio.time;
            }

            RefreshTimeThings();
        }
    }
    
    public void RefreshTimeThings() {
        GameObject.Find("mus pos slider thing").GetComponent<Slider>().value = status == 1 ? GetPosZeroToOne() : GetPosZeroToOneST();
        GameObject.Find("timetext").GetComponent<Text>().text = "B" + GetCurrentBeat().ToString("#.00") + " - S" + GetCurrentTime().ToString("#.00");
    }

    public void TogglePlayPause() { 
        if (status != 1) {
            Play();
        } else {
            Pause();
        }
    }

    public void Play() {
        if (isEditor) {
            AudioSource gaudio = GameObject.Find("song").GetComponent<AudioSource>();
            if (startTime == gaudio.clip.length) return; //if you're dumb, you're dumb
            status = 1;
            AudioSource music = GameObject.Find("song").GetComponent<AudioSource>(); //yada yada, add song loading or w/e
            music.time = startTime;
            music.Play();
            startTime = (float) AudioSettings.dspTime - startTime;
            SCRE_GameController_Script gcont = GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script;
            gcont.ToggleBumButtons(false);
        } else {
            status = 1;
            AudioSource music = GameObject.Find("song").GetComponent<AudioSource>(); //yada yada, add song loading or w/e
            music.Play();
            startTime = (float) AudioSettings.dspTime;
        }
    }

    public void Pause() {
        status = 0;
        startTime = (float) AudioSettings.dspTime - startTime;
        AudioSource music = GameObject.Find("song").GetComponent<AudioSource>(); //yada yada, add song loading or w/e
        music.Pause();
        if (isEditor) {
            SCRE_GameController_Script gcont = GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script;
            gcont.ToggleBumButtons(true);
            startTime = BeatToSecond(Mathf.Round(GetCurrentBeat()));
            music.time = startTime;
            RefreshTimeThings();
        }
    }

    public float GetCurrentTime() {
        switch (status) {
            case -1:
                return startTime;
            case 0:
                return startTime;
            case 1:
                return (float) (AudioSettings.dspTime - startTime) + offset;
            default:
                return 0;
        }
    }

    public float GetCurrentBeat() {
        return GetCurrentTime()*(bpm/60);
    }

    public float BeatToSecond(float beat) {
        return beat/(bpm/60);
    }

    public float SecondToBeat(float second) {
        return second*(bpm/60);
    }

    public void SetBeat(float beat) {
        startTime = BeatToSecond(beat);
        RefreshTimeThings();
    }

    public void ChangeBeat(float beats, float snap) {
        startTime += BeatToSecond(beats*(4/snap));
        RefreshTimeThings();
    }

    public float GetPosZeroToOne() {
        AudioSource music = GameObject.Find("song").GetComponent<AudioSource>();
        return music.time / music.clip.length;
    }

    public float GetPosZeroToOneST() {
        AudioSource music = GameObject.Find("song").GetComponent<AudioSource>();
        return startTime / music.clip.length;
    }

    public int GetAmountOfBeats() {
        AudioSource music = GameObject.Find("song").GetComponent<AudioSource>();
        float len = music.clip.length;
        return (int) Mathf.Ceil((len/(60/bpm))/4);
    }

    public void SetTimeSliderBar(float time) { //only works in editor and when paused, for the time slider bar thing
        if (status == 1) return;
        AudioSource gaudio = GameObject.Find("song").GetComponent<AudioSource>();
        startTime = gaudio.clip.length*time;
        RefreshTimeThings();
    }

    public void LoadSongFromFile() {
        FileBrowser.SetFilters( false, new FileBrowser.Filter( "Ogg files (and also mp3)", ".ogg", ".mp3" ));
        if (FileBrowser.CheckPermission() != FileBrowser.Permission.Granted) FileBrowser.RequestPermission();
        StartCoroutine( DoTheThingWhereWeOpenAnDialogAndLoadTheSongForTheEditorOkThankYou() );
    }

    public void ClampAudioPos() { //only works when paused! and probably only in the editor, too.
        AudioSource gaudio = GameObject.Find("song").GetComponent<AudioSource>();
        if (startTime > gaudio.clip.length) {
            startTime = gaudio.clip.length;
        } else if (startTime < 0) {
            startTime = 0;
        }
        RefreshTimeThings();
    }

    IEnumerator DoTheThingWhereWeOpenAnDialogAndLoadTheSongForTheEditorOkThankYou()
	{
		yield return FileBrowser.WaitForLoadDialog( FileBrowser.PickMode.Files, false, null, null, "Load Song", "Swag" );

		if( FileBrowser.Success ) {
            string path = FileBrowser.Result[0].ToString();
            Debug.Log(path);
            string extension = path.Substring(path.Length - 3, 3);
            UnityWebRequest www;
            try {
                if (path.Substring(0, 10) == "content://") { //BROKEN, doesnt work, to fix stare at this until it works https://stackoverflow.com/a/59025269 https://github.com/WulfMarius/NAudio-Unity
                    byte[] file = FileBrowserHelpers.ReadBytesFromFile(path);
                    float[] samples = new float[file.Length * 4]; //size of a float is 4 bytes
 
                    Buffer.BlockCopy(file, 0, samples, 0, file.Length);
                    
                    int channels = 2;
                    int sampleRate = 44100; //assume 44100;
                    
                    AudioClip clip = AudioClip.Create("ClipName", samples.Length, channels, sampleRate, false);
                    clip.SetData(samples, 0);
                    clip.LoadAudioData();

                    AudioSource music = GameObject.Find("song").GetComponent<AudioSource>();
                    music.clip = clip;
                    (GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script).GenerateBeatLines();
                    yield break;
                } else {
                    Debug.Log(path.Substring(0, 10));
                    path = "file://" + path;
                } 
            } catch {
                path = "file://" + path;
            }
            if (extension == "ogg") {
                www = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.OGGVORBIS);
            } else if (extension == "mp3") {
                www = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.MPEG);
            } else {
                www = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.UNKNOWN);
            }
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError) {
                Debug.Log(www.error);
            } else {
                AudioSource music = GameObject.Find("song").GetComponent<AudioSource>();
                music.clip = DownloadHandlerAudioClip.GetContent(www);
            }

            (GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script).GenerateBeatLines();
		}
	}

    //https://docs.unity3d.com/ScriptReference/AudioSource.GetSpectrumData.html
}
