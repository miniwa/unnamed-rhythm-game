﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SCR_Line_Script : MonoBehaviour
{
    Vector3[] mVerts;
    Vector2[] mUVs;
    int[] mTris;

    public float startNoteA = 0;
    public float startNoteB = 1;
    public float endNoteA = 2;
    public float endNoteB = 3;
    public float length = 2;
    public float noteTime = 8;
    public float xMod = 2;
    public float xModModifier = 1;

    public Texture staticTexture;
    public Texture funnyTexture;

    SCR_GameController_Audio gaudio;
    SCRE_GameController_Script geditor;

    bool isEditor = false;
    
    public bool isARealNote = false;

    Mesh mesh;

    // Start is called before the first frame update
    void Start() {
        isEditor = SceneManager.GetActiveScene().name == "EditorScene";
        if (isEditor) geditor = GameObject.Find("GameController").GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script;
        gaudio = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        RecalculateMesh();
        if (transform.parent == null) {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<MeshCollider>().enabled = false;
        } else {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<MeshCollider>().enabled = true;
            isARealNote = true;
        }
    }

    public void RecalculateMesh() {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshCollider>().sharedMesh = mesh;
        
        if (startNoteA == endNoteA && startNoteB == endNoteB) {
            GetComponent<MeshRenderer>().material.SetTexture("_MainTex", staticTexture);
        } else {
            GetComponent<MeshRenderer>().material.SetTexture("_MainTex", funnyTexture);//set texture b
        }
        GetComponent<MeshRenderer>().material.renderQueue = 3999;

        mVerts = new Vector3[4];
        mUVs = new Vector2[4];
        mTris = new int[6];

        //all we need to touch, apparently
        mVerts[0] = new Vector3(endNoteA-1, 0.0f, length);
        mVerts[1] = new Vector3(endNoteB-1, 0.0f, length);
        mVerts[2] = new Vector3(startNoteA-1, 0.0f, 0);
        mVerts[3] = new Vector3(startNoteB-1, 0.0f, 0);

        mUVs[0] = new Vector2(startNoteA-1, 0.0f);
        mUVs[1] = new Vector2(startNoteB-1, 0.0f);
        mUVs[2] = new Vector2(endNoteA-1, length);
        mUVs[3] = new Vector2(endNoteB-1, length);

        mTris[0] = 0;
        mTris[1] = 1;
        mTris[2] = 3;

        mTris[3] = 0;
        mTris[4] = 3;
        mTris[5] = 2;

        mesh.vertices = mVerts;
        mesh.uv = mUVs;
        mesh.triangles = mTris;

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }

    // Update is called once per frame
    void Update() {
        if (isEditor) {
            transform.position = new Vector3(transform.position.x,transform.position.y,
            (geditor.editorZoom*noteTime*4)  -  ((geditor.editorZoom*gaudio.GetCurrentTime())*(gaudio.bpm/60)));
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, geditor.editorZoom);
        } else {
            if (transform.position.z < -10-length) {
                Destroy(gameObject);
            }
            transform.position = new Vector3(transform.position.x,transform.position.y,
            0.95f  +  (xModModifier*xMod*noteTime*4)  -  (xModModifier*xMod*(gaudio.GetCurrentTime())*(gaudio.bpm/60)));
            //question if the 0.95f is needed? it might not be, actually...
        }
        
        if (transform.position.z > 29.5-length/2 && !isEditor) {
            GetComponent<MeshRenderer>().enabled = false;
        } else if (isARealNote) {
            GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public void SetSel(bool set) {
        var render = GetComponent<Renderer>();
        if (set) {
            if (startNoteA == endNoteA && startNoteB == endNoteB) {
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", funnyTexture);
            } else {
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", staticTexture);//set texture b
            }
        } else {
            if (startNoteA == endNoteA && startNoteB == endNoteB) {
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", staticTexture);
            } else {
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", funnyTexture);//set texture b
            }
        }
    }
}