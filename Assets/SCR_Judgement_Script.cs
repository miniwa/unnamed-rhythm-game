﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Judgement_Script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() {
        transform.Find("Perfect").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Great").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Good").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Miss").GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void SetJudgement(int judge) {
        transform.Find("Perfect").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Great").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Good").GetComponent<SpriteRenderer>().enabled = false;
        transform.Find("Miss").GetComponent<SpriteRenderer>().enabled = false;
        switch (judge) {
            case 0:
                transform.Find("Miss").GetComponent<SpriteRenderer>().enabled = true;
                break;
            case 1:
                transform.Find("Good").GetComponent<SpriteRenderer>().enabled = true;
                break;
            case 2:
                transform.Find("Great").GetComponent<SpriteRenderer>().enabled = true;
                break;
            case 3:
                transform.Find("Perfect").GetComponent<SpriteRenderer>().enabled = true;
                break;
        }
    }
}
