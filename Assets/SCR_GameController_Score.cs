﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GameController_Score : MonoBehaviour
{
    public int maxNotes = 0;

    public int perfects = 0;

    public int greats = 0;

    public int goods = 0;

    public int misses = 0;

    public int lastJudge = 0;

    public int isFc = 1; // -1 = NO, 0 = FC, 1 = PFC

    public int combo = 0;

    // Start is called before the first frame update
    void Start() {
        SCR_ScoreText_Script scorething = GameObject.Find("Score Text").GetComponent(typeof(SCR_ScoreText_Script)) as SCR_ScoreText_Script;
        scorething.SetScore(GetScoreMeimei());
    }

    // Update is called once per frame
    void Update() {
    }

    public void AddJudge(int type) { // 0 = miss, 1 = good, 2 = great, 3 = perfect
        switch (type) {
            case 0:
                misses++;
                if (isFc == 1 || isFc == 0) isFc = -1;
                combo = 0;
                break;
            case 1:
                goods++;
                if (isFc == 1) isFc = 0;
                combo++;
                break;
            case 2:
                greats++;
                if (isFc == 1) isFc = 0;
                combo++;
                break;
            case 3:
                perfects++;
                combo++;
                break;
        }
        SCR_Judgement_Script judgescr = GameObject.Find("Judgement").GetComponent(typeof(SCR_Judgement_Script)) as SCR_Judgement_Script;
        judgescr.SetJudgement(type);
        SCR_ScoreText_Script scorething = GameObject.Find("Score Text").GetComponent(typeof(SCR_ScoreText_Script)) as SCR_ScoreText_Script;
        scorething.SetScore(GetScoreMeimei());
        SCR_ComboText_Script combothing = GameObject.Find("Combo Text").GetComponent(typeof(SCR_ComboText_Script)) as SCR_ComboText_Script;
        combothing.SetCombo(combo, isFc);
    }

    public int GetScore() {
        return (int) Mathf.Ceil(((perfects + greats*0.75f + goods*0.5f)/maxNotes)*1000000);
    }

    public int GetScoreMeimei() {
        return 1000000 - (int) Mathf.Floor((misses + goods*0.5f + greats*0.25f)/maxNotes*1000000);
    }

    public float GetPercentInTheGrooveLike() {
        return ((perfects + greats*0.75f + goods*0.5f)/maxNotes)*100;
    }

    public float GetPercentOsuLike() {
        return ((float) (perfects + greats*0.75f + goods*0.5f) / (float) (perfects+greats+goods+misses))*100f;
    }
}
