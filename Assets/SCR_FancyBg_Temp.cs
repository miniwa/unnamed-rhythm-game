﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_FancyBg_Temp : MonoBehaviour
{

    SCR_GameController_Audio gaudio;
    // Start is called before the first frame update
    void Start()
    {
        gaudio = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < transform.childCount; i++) {
            GameObject bg = transform.GetChild(i).gameObject;
            bg.transform.localPosition = new Vector3(0, 0, (i == 3 ? 0.1f : 0) + (Mathf.Sin(gaudio.GetCurrentBeat()/2+i)*0.03f));
        }
    }
}
