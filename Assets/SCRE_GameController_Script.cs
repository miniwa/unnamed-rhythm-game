﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser;


public class SCRE_GameController_Script : MonoBehaviour
{
    public SCR_Note_Script lastNote;
    public GameObject lastNoteGo;
    public SCR_Line_Script lastSlide;
    public GameObject lastSlideGo;
    public List<SCR_Note_Script> lastNotes;
    public List<GameObject> lastNotesGo;
    public List<SCR_Line_Script> lastSlides;
    public List<GameObject> lastSlidesGo;
    
    int last = 0;
    float beatSnap = 4;
    public float editorZoom = 1;
    public int touchMode = 0; // 0 = selection mode, 1 = add mode, 2 = multi select mode

    // Start is called before the first frame update
    void Start() {
        SetBpm(180);
        SetNpLpOn(false,false);
        GenerateBeatLines();
    }

    // Update is called once per frame
    void Update() {
        Ray ray;
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began) {
            //handle touch input . . .
            ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
        } else if (Input.GetMouseButtonDown(0)) {
            //handle mouse input....
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        } else {
            return;
        }
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            var selection = hit.transform;
            if (selection.CompareTag("Note") && touchMode == 0 || touchMode == 2) {
                if (lastNote != null) lastNote.SetSel(false);
                if (lastSlide != null) lastSlide.SetSel(false);
                lastNote = selection.GetComponent(typeof(SCR_Note_Script)) as SCR_Note_Script;
                lastNoteGo = selection.gameObject;
                lastNote.SetSel(true);
                last = 0;
                SetNpLpOn(true,false);
            } else if (selection.CompareTag("Slide") && touchMode == 0 || touchMode == 2) {
                if (lastNote != null) lastNote.SetSel(false);
                if (lastSlide != null) lastSlide.SetSel(false);
                lastSlide = selection.GetComponent(typeof(SCR_Line_Script)) as SCR_Line_Script;
                lastSlideGo = selection.gameObject;
                lastSlide.SetSel(true);
                last = 1;
                SetNpLpOn(false,true);
            } else if (selection.CompareTag("beatLine") && touchMode == 1) {
                float key = 0;
                float touchNum = 0.125f;
                float cursorPos = 69;
                if (Input.touchCount > 0) {   
                    cursorPos = Camera.main.ScreenToWorldPoint(new Vector3( Input.touches[0].position.x, 0, 1f )).x;
                } else if (Input.GetMouseButtonDown(0)) {
                    cursorPos = Camera.main.ScreenToWorldPoint(new Vector3( Input.mousePosition.x, 0, 1f )).x;
                }
                if (cursorPos < touchNum * -3) {
                    key = 1;
                } else if (cursorPos < touchNum * -2) {
                    key = 2;
                } else if (cursorPos < touchNum * -1) {
                    key = 3;
                } else if (cursorPos <= touchNum * -0) {
                    key = 4;
                } else if (cursorPos > touchNum * 3) {
                    key = 8;
                } else if (cursorPos > touchNum * 2) {
                    key = 7;
                } else if (cursorPos > touchNum * 1) {
                    key = 6;
                } else if (cursorPos > touchNum * 0) {
                    key = 5;
                }
                AddNote(selection.GetComponent<SCRE_BeatLine_Script>().noteTime, 1, 1, key, 0);
            }
        }
    }
    
    public void SetSelFalse(bool lastNoteDel, bool lastSlideDel, bool all) {
        if (touchMode != 2)  {
            if (lastNoteDel && lastNote != null) lastNote.SetSel(false);
            if (lastSlideDel && lastSlide != null) lastSlide.SetSel(false);
        } else {
            if (lastNoteDel) lastNotes = new List<SCR_Note_Script>();
            if (lastSlideDel) lastSlides = new List<SCR_Line_Script>();
        }
        if (all) {
            if (lastNote != null) lastNote.SetSel(false);
            if (lastSlide != null) lastSlide.SetSel(false);
            lastNotes = new List<SCR_Note_Script>();
            lastSlides = new List<SCR_Line_Script>();
        }
    }

    public void SetBpmStr(string bpm) {
        try {
            float fml = float.Parse(bpm);
            SetBpm(fml);
        } catch {}
    }

    void SetBpm(float bpm) {
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        gaudio.bpm = bpm;
        GameObject.Find("mainBpm").GetComponent<InputField>().text = bpm.ToString();
        GenerateBeatLines();
    }

    public void AddNote(float noteTime = -1, float xMod = 1, float keys = 1, float key = 1, int type = 0) { //float noteTime, float speedModifier, float keys, float key, int type
        SetSelFalse(true, true, false);
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        SCR_Note_Script note = Instantiate(GameObject.Find("Note"), this.transform).GetComponent(typeof(SCR_Note_Script)) as SCR_Note_Script;
        note.noteTime = noteTime == -1 ? gaudio.GetCurrentBeat()/4 : noteTime;
        note.enable = true;
        note.xMod = 1;
        note.xModModifier = xMod;
        note.keys = keys;
        note.key = key;
        note.type = type;
        lastNote = note;
        lastNoteGo = note.transform.gameObject;
        note.SetSel(true);
        SetNpLpOn(true,false);
    }
    
    public void AddNoteButton() {AddNote();}

    public void AddLine(float noteTime = -1, float xMod = 1, float length = 2, float startNoteA = 1, float startNoteB = 3, float endNoteA = 3, float endNoteB = 5) {
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        SCR_Line_Script line = Instantiate(GameObject.Find("Line"), this.transform).GetComponent(typeof(SCR_Line_Script)) as SCR_Line_Script;
        line.noteTime = noteTime == -1 ? gaudio.GetCurrentBeat()/4 : noteTime;
        SetSelFalse(true, true, false);
        line.xMod = 1;
        line.xModModifier = xMod;
        line.length = length;
        line.startNoteA = startNoteA;
        line.startNoteB = startNoteB;
        line.endNoteA = endNoteA;
        line.endNoteB = endNoteB;
        lastSlide = line;
        lastSlideGo = line.transform.gameObject;
        line.SetSel(true);
        SetNpLpOn(false,true);
    }
    
    public void AddLineButton() {AddLine();}

    public void DeleteNote() {
        if (last == 0) Destroy(lastNoteGo);
        else if (last == 1) Destroy(lastSlideGo);
        lastNote = null;
        lastNoteGo = null;
        lastSlide = null;
        lastSlideGo = null;
        SetNpLpOn(false,false);    
    }

    void SetNpLpOn(bool np, bool lp) {
        if (np) {
            GameObject.Find("note properties").transform.localScale = new Vector3(1,1,1);
            UpdateNoteProperties();
        } else {
            GameObject.Find("note properties").transform.localScale = new Vector3(0,0,0);
        }
        if (lp) {
            GameObject.Find("line properties").transform.localScale = new Vector3(1,1,1);
            UpdateLineProperties();
        } else {
            GameObject.Find("line properties").transform.localScale = new Vector3(0,0,0);
        }
    }

    public void GenerateBeatLines() {
        GameObject[] gos = GameObject.FindGameObjectsWithTag("beatLine");
        foreach(GameObject go in gos) Destroy(go);
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        for (int i = 0; i < gaudio.GetAmountOfBeats() / (4/beatSnap); i++) {
            GameObject beatLoine = Instantiate(GameObject.Find("Beat line"), this.transform);
            beatLoine.tag = "beatLine";
            SCRE_BeatLine_Script beatLoineScr = beatLoine.GetComponent(typeof(SCRE_BeatLine_Script)) as SCRE_BeatLine_Script;
            beatLoineScr.noteTime = i * (4/beatSnap);
        }
    }
    
    public void ToggleTouchMode() {
        Text theText = GameObject.Find("modeswitchtext").GetComponent<Text>();
        if (touchMode == 0) {
            touchMode = 1;
            theText.text = "add mode";
            SetSelFalse(false, false, true);
            SetNpLpOn(false, false);
        } else if (touchMode == 1) {
            touchMode = 2;
            theText.text = "multi select";
        } else {
            touchMode = 0;
            theText.text = "select mode";
        }
    }

    public void SetDeviceOffset(string offset) {
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        try {
            float sure = float.Parse(offset)/1000;
            gaudio.offset = sure;
        } catch {}
    }

    public void SetEditorZoom(string zoom) {
        try {
            float sure = float.Parse(zoom);
            editorZoom = sure;
        } catch {}
    }

    //all the note functions

    public void UpdateNoteProperties() {
        //do shit here i guess
        GameObject.Find("noteprokeyinput").GetComponent<InputField>().text = lastNote.key.ToString();
        GameObject.Find("noteprokeysinput").GetComponent<InputField>().text = lastNote.keys.ToString();
        GameObject.Find("notepronotetimeinput").GetComponent<InputField>().text = lastNote.noteTime.ToString();
        GameObject.Find("noteprospeedmodinput").GetComponent<InputField>().text = lastNote.xModModifier.ToString();
    }

    public void SetNoteLane(string lol) {
        switch (lol) {
            case "+0.5":
                lastNote.key += 0.5f;
                UpdateNoteProperties();
                break;
            case "-0.5":
                lastNote.key -= 0.5f;
                UpdateNoteProperties();
                break;
            default:
                try {
                    float sure = float.Parse(lol);
                    lastNote.key = sure;
                } catch {}
                break;
        }
    }

    public void SetNoteBeat(string lol) {
        switch (lol) {
            case "+1b":
                lastNote.noteTime += 4/beatSnap;
                UpdateNoteProperties();
                break;
            case "-1b":
                lastNote.noteTime -= 4/beatSnap;
                UpdateNoteProperties();
                break;
            default:
                try {
                    float sure = float.Parse(lol);
                    lastNote.noteTime = sure;
                } catch {}
                break;
        }
    }

    public void SetNoteSpeedModifier(string lol) {
        try {
            float sure = float.Parse(lol);
            lastNote.xModModifier = sure;
            if (lol == lastNote.xModModifier.ToString()) UpdateNoteProperties();
        } catch {}
    }

    public void SetNoteKeys(string lol) {
        switch (lol) {
            case "+1":
                lastNote.keys += 1;
                UpdateNoteProperties();
                break;
            case "-1":
                lastNote.keys -= 1;
                UpdateNoteProperties();
                break;
            default:
                try {
                    float sure = float.Parse(lol);
                    lastNote.keys = sure;
                } catch {}
                break;
        }
        lastNote.UpdateKeys();
    }

    public void SetNoteType(string type) {
        switch (type) {
            case "tap":
                lastNote.type = 0;
                lastNote.UpdateTexture(0);
                break;
            case "slide":
                lastNote.type = 1;
                lastNote.UpdateTexture(1);
                break;
            case "flick":
                lastNote.type = 2;
                lastNote.UpdateTexture(2);
                break;
            case "lift":
                lastNote.type = 3;
                lastNote.UpdateTexture(3);
                break;
            case "dream":
                lastNote.type = 4;
                lastNote.UpdateTexture(4);
                break;
        }
    }

    public void DeselectAll() {
        SetSelFalse(false, false, true);
        SetNpLpOn(false, false);
    }

    //line stuff

    public void UpdateLineProperties() {
        GameObject.Find("lineprostartinputa").GetComponent<InputField>().text = lastSlide.startNoteA.ToString();
        GameObject.Find("lineprostartinputb").GetComponent<InputField>().text = lastSlide.startNoteB.ToString();
        GameObject.Find("lineproendinputa").GetComponent<InputField>().text = lastSlide.endNoteA.ToString();
        GameObject.Find("lineproendinputb").GetComponent<InputField>().text = lastSlide.endNoteB.ToString();
        GameObject.Find("linepronotetimeinput").GetComponent<InputField>().text = lastSlide.noteTime.ToString();
        GameObject.Find("lineprolengthinput").GetComponent<InputField>().text = lastSlide.length.ToString();
        GameObject.Find("lineprospeedmodinput").GetComponent<InputField>().text = lastSlide.xModModifier.ToString();
    }

    public void SetLineSpeedModifier(string lol) {
        try {
            float sure = float.Parse(lol);
            lastSlide.xModModifier = sure;
            if (lol == lastSlide.xModModifier.ToString()) UpdateLineProperties();
        } catch {}
    }
    
    public void SetLineStartA(string lol) {
        try {
            float sure = float.Parse(lol);
            lastSlide.startNoteA = sure;
            if (lol == lastSlide.startNoteA.ToString()) UpdateLineProperties();
        } catch {}
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }
    
    public void SetLineStartB(string lol) {
        try {
            float sure = float.Parse(lol);
            lastSlide.startNoteB = sure;
            if (lol == lastSlide.startNoteB.ToString()) UpdateLineProperties();
        } catch {}
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }
    
    public void SetLineEndA(string lol) {
        try {
            float sure = float.Parse(lol);
            lastSlide.endNoteA = sure;
            if (lol == lastSlide.endNoteA.ToString()) UpdateLineProperties();
        } catch {}
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }
    
    public void SetLineEndB(string lol) {
        try {
            float sure = float.Parse(lol);
            lastSlide.endNoteB = sure;
            if (lol == lastSlide.endNoteB.ToString()) UpdateLineProperties(); //this seems fucking stupid, but basically solves everything
        } catch {}
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }
    
    public void SetLineBeat(string lol) {
        switch (lol) {
            case "+1b":
                lastSlide.noteTime += 4/beatSnap;
                UpdateLineProperties();
                break;
            case "-1b":
                lastSlide.noteTime -= 4/beatSnap;
                UpdateLineProperties();
                break;
            default:
                try {
                    float sure = float.Parse(lol);
                    lastSlide.noteTime = sure;
                } catch {}
                break;
        }
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }
    
    public void SetLineLength(string lol) {
        switch (lol) {
            case "+1b":
                lastSlide.length += 4/beatSnap;
                UpdateLineProperties();
                break;
            case "-1b":
                lastSlide.length -= 4/beatSnap;
                UpdateLineProperties();
                break;
            default:
                try {
                    float sure = float.Parse(lol);
                    lastSlide.length = sure;
                } catch {}
                break;
        }
        lastSlide.RecalculateMesh();
        lastSlide.SetSel(true);
    }

    //stuff for music controls
    public void ChangeMusicBeat(string lol) {
        SCR_GameController_Audio audios = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        switch (lol) {
            case "+4":
                audios.ChangeBeat(4, beatSnap);
                break;
            case "+1":
                audios.ChangeBeat(1, beatSnap);
                break;
            case "-1":
                audios.ChangeBeat(-1, beatSnap);
                break;
            case "-4":
                audios.ChangeBeat(-4, beatSnap);
                break;
        }
        if (audios.GetPosZeroToOneST() > 1 || audios.GetPosZeroToOneST() < 0) {
            audios.ClampAudioPos();
        }
    }

    public void SetBeatSnap(string snap) {
        try {
            beatSnap = float.Parse(snap); 
            GenerateBeatLines();
        } catch {}
    }

    public void ToggleBumButtons(bool bottom) {
        GameObject lolBottom = GameObject.Find("bottom");
        if (bottom) {
            lolBottom.transform.Find("beat-1").GetComponent<Button>().interactable = true;
            lolBottom.transform.Find("beat-4").GetComponent<Button>().interactable = true;
            lolBottom.transform.Find("beat+1").GetComponent<Button>().interactable = true;
            lolBottom.transform.Find("beat+4").GetComponent<Button>().interactable = true;
            lolBottom.transform.Find("mus pos slider thing").GetComponent<Slider>().interactable = true;
        } else {
            lolBottom.transform.Find("beat-1").GetComponent<Button>().interactable = false;
            lolBottom.transform.Find("beat-4").GetComponent<Button>().interactable = false;
            lolBottom.transform.Find("beat+1").GetComponent<Button>().interactable = false;
            lolBottom.transform.Find("beat+4").GetComponent<Button>().interactable = false;
            lolBottom.transform.Find("mus pos slider thing").GetComponent<Slider>().interactable = false;
        }
    }
    
    [System.Serializable]
    public class SavedNote {
        public float keys = 1;
        public float key = 1;
        public float noteTime = 8;
        public int type = 0;
        public float xModModifier = 1;
    }
    
    [System.Serializable]
    public class SavedSlide {
        public float startNoteA = 0;
        public float startNoteB = 1;
        public float endNoteA = 2;
        public float endNoteB = 3;
        public float length = 2;
        public float noteTime = 8;
        public float xModModifier = 1;
    }

    [System.Serializable]
    public class Chart {
        public float mainBpm;
        public string songName;
        public string songArtist;
        public string charter;
        public string blurbNavi;
        public string blurbBlurb;
        public int difficulty;
        public int difficultyNumber;
        public List<SavedNote> notes;
        public List<SavedSlide> slides;
    }
    
    public void SaveChart() {
        SCR_GameController_Audio gaudio = GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        Chart savedLevel = new Chart();
        string version = "DNw0"; //we dont have bpm changes yet, so therefore this is cant be called v1 yet
        savedLevel.mainBpm = gaudio.bpm; //also, versions: DNv0 (only in a build seo got once)
        savedLevel.songName = "null";                   // DNw0 (the above, but with difficulty numbers)
        savedLevel.songArtist = "null";
        savedLevel.charter = "null";
        savedLevel.blurbBlurb = "null";
        savedLevel.blurbNavi = "null";
        savedLevel.difficulty = 0; // 0 = EZ, 1 = NM, 2 = HD, 3 = DM, 4 = NM
        savedLevel.difficultyNumber = 1; //how are we gonna do diffs? i'll just assign this random number to 1 for now.
        savedLevel.notes = new List<SavedNote>();
        GameObject[] notes = GameObject.FindGameObjectsWithTag("Note");
        foreach(GameObject note in notes)
        {
            SCR_Note_Script noteScript = note.GetComponent(typeof(SCR_Note_Script)) as SCR_Note_Script;
            if (!noteScript.isARealNote) continue;
            SavedNote saveNote = new SavedNote();
            saveNote.key = noteScript.key;
            saveNote.keys = noteScript.keys;
            saveNote.type = noteScript.type;
            saveNote.noteTime = noteScript.noteTime;
            saveNote.xModModifier = noteScript.xModModifier;
            savedLevel.notes.Add(saveNote);
        }
        savedLevel.slides = new List<SavedSlide>();
        GameObject[] slides = GameObject.FindGameObjectsWithTag("Slide");
        foreach(GameObject slide in slides) {
            SCR_Line_Script slideScript = slide.GetComponent(typeof(SCR_Line_Script)) as SCR_Line_Script;
            if (!slideScript.isARealNote) continue;
            SavedSlide saveSlide = new SavedSlide();
            saveSlide.length = slideScript.length;
            saveSlide.noteTime = slideScript.noteTime;
            saveSlide.endNoteA = slideScript.endNoteA;
            saveSlide.endNoteB = slideScript.endNoteB;
            saveSlide.startNoteA = slideScript.startNoteA;
            saveSlide.startNoteB = slideScript.startNoteB;
            saveSlide.xModModifier = slideScript.xModModifier;
            savedLevel.slides.Add(saveSlide);
        }

        string jsonOut = JsonUtility.ToJson(savedLevel);
        
        FileBrowser.SetFilters( false, new FileBrowser.Filter( "Dream notes chart files (also known as a renamed json file)", ".dnchart" ));
        if (FileBrowser.CheckPermission() != FileBrowser.Permission.Granted) FileBrowser.RequestPermission();
        StartCoroutine( SaveChartDialog(version + jsonOut) );
    }
    
    IEnumerator SaveChartDialog(string text)
	{
		yield return FileBrowser.WaitForSaveDialog( FileBrowser.PickMode.Files, false, null, null, "Save Chart", "Swag" );

		if ( FileBrowser.Success ) {
            string path = FileBrowser.Result[0].ToString();
            FileBrowserHelpers.WriteTextToFile( path, text );
		}
	}
    
    public void LoadChart() {
        FileBrowser.SetFilters( false, new FileBrowser.Filter( "Dream notes chart files (also known as a renamed json file)", ".dnchart" ));
        if (FileBrowser.CheckPermission() != FileBrowser.Permission.Granted) FileBrowser.RequestPermission();
        StartCoroutine( LoadChartDialog() );
    }
    
    IEnumerator LoadChartDialog()
    {
        yield return FileBrowser.WaitForLoadDialog( FileBrowser.PickMode.Files, false, null, null, "Load Chart", "Swag" );

        if( FileBrowser.Success ) {
            
            //clean every fucking note from the thing
            GameObject[] notes = GameObject.FindGameObjectsWithTag("Note");
            foreach(GameObject note in notes) {
                SCR_Note_Script noteScript = note.GetComponent(typeof(SCR_Note_Script)) as SCR_Note_Script;
                if (noteScript.isARealNote) Destroy(note);
            }
            GameObject[] slides = GameObject.FindGameObjectsWithTag("Slide");
            foreach(GameObject slide in slides) {
                SCR_Line_Script slideScript = slide.GetComponent(typeof(SCR_Line_Script)) as SCR_Line_Script;
                if (slideScript.isARealNote) Destroy(slide);
            }
            string path = FileBrowser.Result[0].ToString();
            string json = FileBrowserHelpers.ReadTextFromFile(path);
            Chart loadedChart = JsonUtility.FromJson<Chart>(json.Substring(4));
            string version = json.Substring(0, 4);
            SetBpm(loadedChart.mainBpm);
            //not gonna worry too much about the song names n stuff....
            if (version == "DNv0") {
                //set diff from placeholder values
            } else {
                //set diff from whats in the json
            }
            foreach (SavedNote note in loadedChart.notes) {
                AddNote(note.noteTime, note.xModModifier, note.keys, note.key, note.type);
            }
            foreach (SavedSlide slide in loadedChart.slides)
            {
                AddLine(slide.noteTime, slide.xModModifier, slide.length, slide.startNoteA, slide.startNoteB, slide.endNoteA, slide.endNoteB);
            }
            GenerateBeatLines();
            SetSelFalse(false, false, true);
            SetNpLpOn(false, false);
        }
    }
}
