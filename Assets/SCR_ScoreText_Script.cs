﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SCR_ScoreText_Script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void SetScore(int score) {
        TextMeshPro textmeshPro = GetComponent<TextMeshPro>();
        textmeshPro.SetText(score.ToString().PadLeft(7, '0'));
        //ok so like. the idea here:
        //* make the text grow slightly
        //or something like that
    }
}
