﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SCR_ComboText_Script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TextMeshPro textmeshPro = GetComponent<TextMeshPro>();
        textmeshPro.color = new Color(179,0,179);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCombo(int combo, int isFc) {
        TextMeshPro textmeshPro = GetComponent<TextMeshPro>();
        textmeshPro.SetText(combo.ToString());
        switch (isFc) {
            case -1:
                textmeshPro.color = Color.white;
                break;
            case 0:
                textmeshPro.color = new Color(81,230,0);
                break;
            case 1:
                textmeshPro.color = new Color(179,0,179);
                break;
        }    
        
        //TODO: similar cool stuff to the score text.
    }
}
