﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SCRE_BeatLine_Script : MonoBehaviour
{
    public float noteTime = -69;
    SCR_GameController_Audio gaudio;
    SCRE_GameController_Script geditor;
    public int touchMode = 0;
    public bool tempDisabled = false;

    public Material green;
    public Material neerg;

    // Start is called before the first frame update
    void Start() {
        geditor = GameObject.Find("GameController").GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script; //this only runs under the editor, so..
        //if not enabled, disable your text renderer and mesh renderer
        gaudio = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        transform.position = new Vector3(0, 0, (noteTime*4)  -  ((gaudio.GetCurrentTime())*(gaudio.bpm/60)));
        transform.Find("Text").GetComponent<TextMeshPro>().SetText(noteTime.ToString("#.00"));
    }

    // Update is called once per frame
    void Update() {
        transform.position = new Vector3(0, transform.position.y, (geditor.editorZoom*noteTime*4)  -  ((geditor.editorZoom*gaudio.GetCurrentTime())*(gaudio.bpm/60)));
        if (transform.tag != "beatLine") {
            GetComponent<MeshRenderer>().enabled = false;
            transform.Find("Text").GetComponent<MeshRenderer>().enabled = false;
        } else {
            GetComponent<MeshRenderer>().enabled = true;
            transform.Find("Text").GetComponent<MeshRenderer>().enabled = true;
        }
        if (geditor.touchMode == 1 && touchMode == 0) {
            GetComponent<MeshRenderer>().material = green;
            transform.localScale = new Vector3(10, 1, 0.1f);
            transform.Find("Text").localScale = new Vector3(0.02f, 0.1f, 0.1f);
            GetComponent<MeshCollider>().enabled = true;
            touchMode = 1;
            tempDisabled = false;
        } else if ((geditor.touchMode == 0 || geditor.touchMode == 2) && touchMode == 1) {
            GetComponent<MeshRenderer>().material = neerg;
            transform.localScale = new Vector3(10, 0.1f, 0.1f);
            transform.Find("Text").localScale = new Vector3(0.02f, 1f, 0.1f);
            GetComponent<MeshCollider>().enabled = false;
            touchMode = 0;
        }
        if (tempDisabled == false && touchMode == 1 && transform.position.z <= -0.5f) {
            GetComponent<MeshCollider>().enabled = false;
            tempDisabled = true;
        } else if (tempDisabled && touchMode == 1 && transform.position.z > -0.5f) {
            GetComponent<MeshCollider>().enabled = true;
            tempDisabled = false;
        }
    }
}
