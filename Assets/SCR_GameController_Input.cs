﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_GameController_Input : MonoBehaviour {
    int lane1 = -1; // -1 = unpressed
    int lane2 = -1; // 0 = pressed
    int lane3 = -1; // 1 = held
    int lane4 = -1; // 2 = flicked
    int lane5 = -1; // 3 = released 
    int lane6 = -1;
    int lane7 = -1;
    int lane8 = -1;

    public float touchNum = 0.225f;

    int[] touchlanes = new int[999];

    // Start is called before the first frame update
    void Start() {
        for (int i = 0; i < 999; i++) touchlanes[i] = -1;
    }

    // Update is called once per frame
    void Update() {
        CheckInput();
        LightBars();
        CheckHits();
    }

    void CheckInput() { //EVENTUALLY make it just l  ,,  check the touchphase instead or something id ont ufcking know just make this less ugly please i beg you
    lane1 = -1; lane2 = -1; lane3 = -1; lane4 = -1; lane5 = -1; lane6 = -1; lane7 = -1; lane8 = -1;
        for (int i = 0; i < Input.touchCount; i++) {
            float x = Input.touches[i].position.x;
            float cursorPos = Camera.main.ScreenToWorldPoint(new Vector3( x, 0, 1f )).x;
            if (Input.touches[i].phase == TouchPhase.Ended) {
                touchlanes[Input.touches[i].fingerId] = -1;
                if (cursorPos < touchNum * -3) {
                    lane1 = 3;
                } else if (cursorPos < touchNum * -2) {
                    lane2 = 3;
                } else if (cursorPos < touchNum * -1) {
                    lane3 = 3;
                } else if (cursorPos <= touchNum * -0) {
                    lane4 = 3;
                } else if (cursorPos > touchNum * 3) {
                    lane8 = 3;
                } else if (cursorPos > touchNum * 2) {
                    lane7 = 3;
                } else if (cursorPos > touchNum * 1) {
                    lane6 = 3;
                } else if (cursorPos > touchNum * 0) {
                    lane5 = 3;
                }
                continue;
            }
            int newLane = 69; //lmao (unity complains if i dont set it to something idk)
            if (cursorPos < touchNum * -3) {
                lane1 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 1;
            } else if (cursorPos < touchNum * -2) {
                lane2 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 2;
            } else if (cursorPos < touchNum * -1) {
                lane3 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 3;
            } else if (cursorPos <= touchNum * -0) {
                lane4 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 4;
            } else if (cursorPos > touchNum * 3) {
                lane8 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 8;
            } else if (cursorPos > touchNum * 2) {
                lane7 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 7;
            } else if (cursorPos > touchNum * 1) {
                lane6 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 6;
            } else if (cursorPos > touchNum * 0) {
                lane5 = Input.touches[i].phase == TouchPhase.Began ? 0 : 1;
                newLane = 5;
            }
            if (touchlanes[Input.touches[i].fingerId] == -1) touchlanes[Input.touches[i].fingerId] = newLane;
            if (newLane != touchlanes[Input.touches[i].fingerId]) {
                //do the funny slide
                if (newLane > touchlanes[Input.touches[i].fingerId]) {
                    for (int o = touchlanes[Input.touches[i].fingerId]; o < newLane; o++) TheOtherCheck(o, 2);
                } else {
                    for (int o = touchlanes[Input.touches[i].fingerId]; o > newLane; o--) TheOtherCheck(o, 2);
                }
                touchlanes[Input.touches[i].fingerId] = newLane;
            }
        }
        //this is the UGLIEST FUCKING SHIT oh my god i hate this
        if (Input.GetButton("Lane 1")) lane1 = Input.GetButtonDown("Lane 1") ? 0 : 1;
        if (Input.GetButton("Lane 2")) lane2 = Input.GetButtonDown("Lane 2") ? 0 : 1;
        if (Input.GetButton("Lane 3")) lane3 = Input.GetButtonDown("Lane 3") ? 0 : 1;
        if (Input.GetButton("Lane 4")) lane4 = Input.GetButtonDown("Lane 4") ? 0 : 1;
        if (Input.GetButton("Lane 5")) lane5 = Input.GetButtonDown("Lane 5") ? 0 : 1;
        if (Input.GetButton("Lane 6")) lane6 = Input.GetButtonDown("Lane 6") ? 0 : 1;
        if (Input.GetButton("Lane 7")) lane7 = Input.GetButtonDown("Lane 7") ? 0 : 1;
        if (Input.GetButton("Lane 8")) lane8 = Input.GetButtonDown("Lane 8") ? 0 : 1;
        if (Input.GetButtonUp("Lane 1")) lane1 = 3;
        if (Input.GetButtonUp("Lane 2")) lane2 = 3;
        if (Input.GetButtonUp("Lane 3")) lane3 = 3;
        if (Input.GetButtonUp("Lane 4")) lane4 = 3;
        if (Input.GetButtonUp("Lane 5")) lane5 = 3;
        if (Input.GetButtonUp("Lane 6")) lane6 = 3;
        if (Input.GetButtonUp("Lane 7")) lane7 = 3;
        if (Input.GetButtonUp("Lane 8")) lane8 = 3;
        if (Input.GetButton("Lane 1 Flick")) {if (Input.GetButtonDown("Lane 1 Flick")) TheOtherCheck(1, 2); lane1 = 1;}
        if (Input.GetButton("Lane 2 Flick")) {if (Input.GetButtonDown("Lane 2 Flick")) TheOtherCheck(2, 2); lane2 = 1;}
        if (Input.GetButton("Lane 3 Flick")) {if (Input.GetButtonDown("Lane 3 Flick")) TheOtherCheck(3, 2); lane3 = 1;}
        if (Input.GetButton("Lane 4 Flick")) {if (Input.GetButtonDown("Lane 4 Flick")) TheOtherCheck(4, 2); lane4 = 1;}
        if (Input.GetButton("Lane 5 Flick")) {if (Input.GetButtonDown("Lane 5 Flick")) TheOtherCheck(5, 2); lane5 = 1;}
        if (Input.GetButton("Lane 6 Flick")) {if (Input.GetButtonDown("Lane 6 Flick")) TheOtherCheck(6, 2); lane6 = 1;}
        if (Input.GetButton("Lane 7 Flick")) {if (Input.GetButtonDown("Lane 7 Flick")) TheOtherCheck(7, 2); lane7 = 1;}
        if (Input.GetButton("Lane 8 Flick")) {if (Input.GetButtonDown("Lane 8 Flick")) TheOtherCheck(8, 2); lane8 = 1;}
    }

    void LightBars() {
        for (int i = 0; i < 8; i++) {
            GameObject.Find("lightning_long_" + i).transform.position = new Vector3(-999999, 0, 15); //todo: JUST HAVE ALL OF THESE EITHER BE ENABLED OR NOT ENABLED NOW THAT YOU HAVE SPECIFICALLY EIGHT OF THEM LMFAO
            GameObject.Find("lightning_short_" + i).transform.position = new Vector3(-999999, 0, 0);
        }
        if (lane1 != -1) { //maybe i could convert all these 8 if statements to a for loop? not sure if you can add a number to a variable name though...
            GameObject.Find("lightning_long_" + 0).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(1-1)), 0, 15);
            GameObject.Find("lightning_short_" + 0).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(1-1)), 0, 0);
        }
        if (lane2 != -1) {
            GameObject.Find("lightning_long_" + 1).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(2-1)), 0, 15);
            GameObject.Find("lightning_short_" + 1).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(2-1)), 0, 0);
        }
        if (lane3 != -1) {
            GameObject.Find("lightning_long_" + 2).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(3-1)), 0, 15);
            GameObject.Find("lightning_short_" + 2).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(3-1)), 0, 0);
        }
        if (lane4 != -1) {
            GameObject.Find("lightning_long_" + 3).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(4-1)), 0, 15);
            GameObject.Find("lightning_short_" + 3).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(4-1)), 0, 0);
        }
        if (lane5 != -1) {
            GameObject.Find("lightning_long_" + 4).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(5-1)), 0, 15);
            GameObject.Find("lightning_short_" + 4).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(5-1)), 0, 0);
        }
        if (lane6 != -1) {
            GameObject.Find("lightning_long_" + 5).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(6-1)), 0, 15);
            GameObject.Find("lightning_short_" + 5).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(6-1)), 0, 0);
        }
        if (lane7 != -1) {
            GameObject.Find("lightning_long_" + 6).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(7-1)), 0, 15);
            GameObject.Find("lightning_short_" + 6).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(7-1)), 0, 0);
        }
        if (lane8 != -1) {
            GameObject.Find("lightning_long_" + 7).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(8-1)), 0, 15);
            GameObject.Find("lightning_short_" + 7).transform.position = new Vector3(-5+(1.25f/2)+(1.25f*(8-1)), 0, 0);
        }
    }

    void CheckHits() {
        if (lane1 == 0 || lane1 == 1 || lane1 == 3) TheOtherCheck(1, lane1);
        if (lane2 == 0 || lane2 == 1 || lane2 == 3) TheOtherCheck(2, lane2);
        if (lane3 == 0 || lane3 == 1 || lane3 == 3) TheOtherCheck(3, lane3);
        if (lane4 == 0 || lane4 == 1 || lane4 == 3) TheOtherCheck(4, lane4);
        if (lane5 == 0 || lane5 == 1 || lane5 == 3) TheOtherCheck(5, lane5);
        if (lane6 == 0 || lane6 == 1 || lane6 == 3) TheOtherCheck(6, lane6);
        if (lane7 == 0 || lane7 == 1 || lane7 == 3) TheOtherCheck(7, lane7);
        if (lane8 == 0 || lane8 == 1 || lane8 == 3) TheOtherCheck(8, lane8);
    }

    void TheOtherCheck(int lane, int type) {
        //long ass for loop goes here
        for (int i = 0; i < transform.childCount; i++) {
            SCR_Note_Script note = transform.GetChild(i).gameObject.GetComponent(typeof(SCR_Note_Script)) as SCR_Note_Script;
            float time = Mathf.Abs(note.TimeRemaining()); //why is this function called .   ABS
                //NO!!!!!
                //muscles
                //r.....
                //NOT CUTE
            if (time > 0.4) {
                //you're not gonna hit a note A FUCKING SECOND ahead of time so we just stop processing
                //if you end up fucking doing that.
                break;
            }
            int noteVal = note.IsHit(lane, type);
            if (noteVal == -1) break;
            if (noteVal == 1) {
                SCR_GameController_Score score = GetComponent(typeof(SCR_GameController_Score)) as SCR_GameController_Score;
                if (type == 1) {
                    //this handles itself later - carry on!
                } else if (type == 2) {
                    score.AddJudge(3);
                } else if (time > 0.2) {
                    score.AddJudge(0);
                } else if (time > 0.1) {
                    score.AddJudge(1);
                } else if (time > 0.06) {
                    score.AddJudge(2);
                } else {
                    score.AddJudge(3);
                }
                if (note.type != 1) Destroy(transform.GetChild(i).gameObject);
                break;
            }
        }
    }
}
