﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SCR_Note_Script : MonoBehaviour
{
    // Start is called before the first frame update

    public Texture normalTex;
    public Texture flickTex;
    public Texture dreamTex;
    public Texture slideTex;
    public Texture liftTex;

    public float xMod = 1;
    public float keys = 1;
    public float key = 1;
    public float noteTime = 8;
    public int type = 0; //0 = tap, 1 = slide, 2 = flick, 3 = lift, 4 = dream (unimplemented, lol)
    public bool enable = false; //todo: remove this ewhenever
    public float xModModifier = 1;
    bool hasBeenHit = false;
    SCR_GameController_Audio gaudio;
    SCRE_GameController_Script geditor;

    bool isEditor = false;

    public bool isARealNote = false;

    void Start() {
        isEditor = SceneManager.GetActiveScene().name == "EditorScene";
        if (isEditor) geditor = GameObject.Find("GameController").GetComponent(typeof(SCRE_GameController_Script)) as SCRE_GameController_Script;
        gaudio = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Audio)) as SCR_GameController_Audio;
        UpdateKeys();
        transform.position = new Vector3(-5+(transform.localScale.x/2+1.25f*(key-1)), 0f, 100f);
        UpdateTexture(type);
        if (transform.parent == null) {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<MeshCollider>().enabled = false;
        } else {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<MeshCollider>().enabled = true;
            isARealNote = true;
        }
    }

    // Update is called once per frame
    void Update() {
        if (isEditor) {
            transform.position = new Vector3(-5+(transform.localScale.x/2+1.25f*(key-1))
            ,transform.position.y,
            (geditor.editorZoom*noteTime*4)  -  ((geditor.editorZoom*gaudio.GetCurrentTime())*(gaudio.bpm/60)));
        } else {
            if (type == 1 && hasBeenHit && TimeRemaining() < 0) {
                SCR_GameController_Score score = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Score)) as SCR_GameController_Score;
                score.AddJudge(3);
                Destroy(gameObject);
            }
            if (TimeRemaining() < -0.2) {
                SCR_GameController_Score score = GameObject.Find("GameController").GetComponent(typeof(SCR_GameController_Score)) as SCR_GameController_Score;
                score.AddJudge(0);
                Destroy(gameObject);
            }
            transform.position = new Vector3(-5+(transform.localScale.x/2+1.25f*(key-1))
            ,transform.position.y,
            0.95f  +  (xModModifier*xMod*noteTime*4)  -  (xModModifier*xMod*(gaudio.GetCurrentTime())*(gaudio.bpm/60)));
        }
        
        if (transform.position.z > 29.5 && !isEditor) {
            GetComponent<MeshRenderer>().enabled = false;
        } else if (isARealNote) {
            GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public int IsHit(int lane, int ntype) { //-1 = yes, but wrong type, 0 = no, 1 = yes
        if (lane >= Mathf.Floor(key) && lane <= Mathf.Ceil(key+keys-1)) {
            if (ntype == type) {
                if (hasBeenHit) {
                    return 0;
                } else if (ntype == type && ((type == 1 || type == 2) ? Mathf.Abs(TimeRemaining()) < 0.2 : true)) {
                    hasBeenHit = true;
                    return 1;
                } else return 0;
            } else {
                return -1;
            }
        } else return 0;
    }

    public float TimeRemaining() {
        return (noteTime*4)/(gaudio.bpm/60) - (gaudio.GetCurrentTime());
    }

    public void SetSel(bool set) {
        var render = GetComponent<Renderer>();
        if (set) {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 90, transform.eulerAngles.z);
        } else {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
        }
    }

    public void UpdateTexture(int what) {
        switch (type) {
            case 0:
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", normalTex);
                break;
            case 1:
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", slideTex); //yo scones hit me up with uhhhh,,, texture
                break;
            case 2:
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", flickTex);
                break;
            case 3:
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", liftTex); //HHHH
                break;
            case 4:
                GetComponent<MeshRenderer>().material.SetTexture("_MainTex", dreamTex);
                break;
        }
    }

    public void UpdateKeys() {
        transform.localScale = new Vector3(1.25f*keys, 0.0001f, 1f);
    }
}
